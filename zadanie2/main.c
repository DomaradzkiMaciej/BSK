#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>


typedef struct __attribute__ ((packed)) myStruct {
    int type;
    int size;
    char name[256];
    int password[8];
} myStruct;


unsigned int a0;
unsigned int a4;
unsigned int a8;
unsigned int ac;


unsigned int decr1(unsigned int key) {
    unsigned int temp;
    unsigned int res;

    res = key ^ a0;
    temp = a8 * 0x2137 + res;
    a0 = a4;
    a4 = a8;
    a8 = ac;
    ac = temp >> 6 | temp * 0x4000000;

    return res;
}


void decr2(unsigned int p) {
    p = p ^ a0;
    a0 = a4;
    a4 = a8;
    a8 = ac;
    ac = p >> 6 | p << 0x1a;
}


unsigned int decr3(unsigned int key, unsigned int b1, unsigned int b2, unsigned int b3) {
    unsigned int temp;

    key = key ^ a0;
    temp = a8 * b1 + key;
    a0 = a8 * b2 + a4;
    a4 = a8;
    a8 = a8 * b3 + ac;
    ac = temp >> 6 | temp * 0x4000000;

    return key;
}


void modGlobal(struct myStruct *buffer) {
    void *name = &(buffer->name);
    while (name != &(buffer->password)) {
        decr2(*(unsigned int *) name);
        name += 4;
    }

    FILE *file = fopen("/proc/self/status", "r");
    if (file == 0) {
        perror("fopen");
        exit(1);
    }
    while (true) {
        char scan1[256];
        char scan2[1024];

        int len1 = fscanf(file, "%[^:]: %s ", scan1, scan2);
        if (len1 == -1) {
            fclose(file);
            return;
        }

        if (len1 != 2) {
            fprintf(stderr, "wrong len: %i\n", len1);
            exit(1);
        }

        size_t len2 = strlen(scan1);
        if ((scan1[len2 - 3] == 'P') && (scan1[len2 - 2] == 'i') && (scan1[len2 - 1] == 'd')) {
            unsigned int pid;
            sscanf(scan2, "%u", &pid);
            decr2(pid);
        }
    }
}


unsigned int reversePassInt(unsigned int passInt) {
    return a0 ^ passInt;
}


int main(int argc, char **argv) {
    myStruct buffer;

    if (argc != 2) {
        printf("usage: %s <input filename>\n", *argv);
        return 0;
    }

    FILE *encryptedFile = fopen(argv[1], "rb");
    if (encryptedFile == 0) {
        perror("fopen");
        return 1;
    }

    memset(&buffer, 0, sizeof(myStruct));

    size_t len = fread(&buffer, sizeof(myStruct), 1, encryptedFile);
    if (len != 1) {
        perror("fread");
        return 1;
    }

    if ((buffer.type != 0xb542020) && (buffer.type != 0x20200b54)) {
        fprintf(stderr, "unrecognized file\n");
        return 1;
    }

    char *type;
    if (buffer.type == 0xb542020) {
        type = "easy";
    } else {
        type = "hard";
    }

    printf("%s mode file, original name %s\n", type, buffer.name);

    a0 = getpid();
    a4 = getppid();
    a8 = 0x9a596725;
    ac = 0x592c3156;

    modGlobal(&buffer);

    unsigned int password[8];
    unsigned int decryptedPassword[8];
    for (int i = 0; i < 8; ++i) {
        password[i] = reversePassInt(buffer.password[i]);
        decryptedPassword[i] = decr1(password[i]);
    }

    if (memcmp(buffer.password, decryptedPassword, 0x20) != 0) {
        fprintf(stderr, "wrong key\n");
        return 1;
    }

    printf("key is correct!!!\ndecrypting...\n");

    FILE *decryptedFile = fopen(buffer.name, "wb");
    if (decryptedFile == (FILE *) 0x0) {
        perror("open");
        return 1;
    }

    for (int i = 0; i < buffer.size; i += 4) {
        unsigned int chunk;
        len = fread(&chunk, 4, 1, encryptedFile);
        if (len != 1) {
            perror("fread");
            return 1;
        }

        if (buffer.type == 0xb542020) {
            chunk = decr1(chunk);
        } else {
            chunk = decr3(chunk, password[2], password[3], password[5]);
        }

        int chLen = buffer.size - i;
        if (4 < chLen) {
            chLen = 4;
        }

        len = fwrite(&chunk, chLen, 1, decryptedFile);
        if (len != 1) {
            perror("fwrite");
            return 1;
        }
    }

    fclose(encryptedFile);
    fclose(decryptedFile);
}
